﻿using System;
using System.Globalization;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace Our.Umbraco.OsmMaps.ValueConverters
{
    public class OsmMapsValueConverter : PropertyValueConverterBase
    {
        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return "Our.Umbraco.OsmMaps".Equals(propertyType.PropertyEditorAlias);
        }

        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source == null || string.IsNullOrWhiteSpace(source.ToString()))
            {
                return null;
            }

            var coordinates = source.ToString().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            return new Models.OsmMapsLocation
            {
                Latitude = decimal.Parse(coordinates[0], CultureInfo.InvariantCulture),
                Longitude = decimal.Parse(coordinates[1], CultureInfo.InvariantCulture),
                ZoomLevel = int.Parse(coordinates[2], CultureInfo.InvariantCulture)
            };
        }
    }
}